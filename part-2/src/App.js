import React from "react";
import { DataB64Renderer } from "./components/DataB64Renderer";
import { DataJsonRenderer } from "./components/DataJsonRenderer";
import { RangeSelector } from "./components/RangeSelector";
import { RemoteDataProvider } from "./components/RemoteDataProvider";

export default function App() {
  return (
    <div className="App">
      {[
        { title: "Missions", url: "missions" },
        { title: "Launches", url: "launches" },
      ].map(({ title, url }) => (
        <div key={title}>
          <h2>{title}</h2>
          <RemoteDataProvider url={`https://api.spacexdata.com/v3/${url}`}>
            {(missions) => (
              <>
                <RangeSelector value={0} min={0} max={10}>
                  {(depth) => (
                    <DataJsonRenderer depth={depth} data={missions} />
                  )}
                </RangeSelector>
                <DataB64Renderer data={missions} />
              </>
            )}
          </RemoteDataProvider>
        </div>
      ))}
    </div>
  );
}
