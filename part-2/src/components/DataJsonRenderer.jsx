import React from "react";

export function DataJsonRenderer({ data, depth = 2 }) {
  depth = parseInt(depth, 10);

  return (
    <pre style={{
      color: "#4A5568",
      border: "1px solid #4A5568",
      backgroundColor: "#E2E8F0",
      borderRadius: "5px",
      padding: "10px",
      overflowY: "hidden"
    }}>
      <code data-testid="json-content">{JSON.stringify(data, null, depth)}</code>
    </pre>
  );
}
