import React from "react";
import PropTypes from "prop-types";

export function encodeData(data) {
  // btoa can't accept any character, so we protect it
  // by using the encodeURIComponent
  // the output would not be strictly identical to
  // the Base 64 encoding of the input.
  return btoa(encodeURIComponent(JSON.stringify(data)));
}

export function DataB64Renderer({ data = "" }) {
  return <code data-testid="encoded-content">{encodeData(data)}</code>;
}

DataB64Renderer.propsType = {
  data: PropTypes.any,
};
