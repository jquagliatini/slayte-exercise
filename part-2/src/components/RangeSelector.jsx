import React from "react";
import PropTypes from "prop-types";

export function RangeSelector({ value = 0, min = 0, max = 10, children }) {
  const [rangeValue, setValue] = React.useState(value);
  return (
    <>
      <label>
        <p>
          Change the current spacing in the JSON rendering:{" "}
          <span
            style={{
              backgroundColor: "#718096",
              color: "white",
              fontWeight: "bold",
              padding: "5px",
              borderRadius: "5px",
            }}
          >
            {rangeValue}
          </span>
        </p>
        <input
          type="range"
          min={min}
          max={max}
          value={rangeValue}
          onChange={({ currentTarget }) => {
            console.log("CHANGE");
            setValue(currentTarget.value);
          }}
          data-testid="range-input"
        />
      </label>
      {children(rangeValue)}
    </>
  );
}

RangeSelector.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.number
}
