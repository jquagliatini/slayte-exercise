import React from "react";
import { render, screen } from "@testing-library/react";
import { DataB64Renderer, encodeData } from "../DataB64Renderer.jsx";

test("it should encode any data into base 64", () => {
  const initialData = { hello: "world" };
  const data = encodeData(initialData);

  render(<DataB64Renderer data={initialData} />);

  expect(screen.getByTestId("encoded-content")).toHaveTextContent(data);
});
