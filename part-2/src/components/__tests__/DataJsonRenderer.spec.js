import React from "react";
import { render } from "@testing-library/react";
import { DataJsonRenderer } from "../DataJsonRenderer.jsx";

test.each([1, 2, 3])("It should render data with a depth of %d", (depth) => {
  const { getByTestId } = render(
    <DataJsonRenderer depth={depth} data={{ hello: "world" }} />
  );

  const spaces = " ".repeat(depth);

  expect(getByTestId("json-content").textContent).toBe(
    `{\n${spaces}"hello": "world"\n}`
  );
});

test("it should render a 'flat' JSON object when 'depth' is 0", () => {
  const { getByTestId } = render(
    <DataJsonRenderer depth={0} data={{ hello: "world" }} />
  );

  expect(getByTestId("json-content").textContent).toBe(`{"hello":"world"}`);
});
