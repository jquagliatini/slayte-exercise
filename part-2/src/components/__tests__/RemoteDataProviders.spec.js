import React from "react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, wait, screen } from "@testing-library/react";

import { RemoteDataProvider } from "../RemoteDataProvider.jsx";

const server = setupServer(
  rest.get("/hello", (_, res, ctx) => {
    return res(ctx.json({ hello: "world" }));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("remote server should return hello world", async () => {
  const response = await fetch("/hello");
  expect(response.ok).toBe(true);

  const { hello } = await response.json();
  expect(hello).toBe("world");
});

test("should retrieve data from remote", async () => {
  render(
    <RemoteDataProvider url="/hello">
      {(data) => <p data-testid="remote-content">{data.hello}</p>}
    </RemoteDataProvider>
  );

  expect(screen.getByTestId("loading-placeholder")).toHaveTextContent(
    "loading..."
  );

  await wait(
    () => {
      try {
        screen.getByTestId("remote-content");
      } catch {
        screen.getByTestId("error-content");
      }
    },
    { timeout: 5_000 }
  );

  expect(screen.getByTestId("remote-content")).toHaveTextContent("world");
}, 5_000);
