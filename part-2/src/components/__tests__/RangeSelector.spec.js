import React from "react";
import { render, fireEvent, wait } from "@testing-library/react";
import { RangeSelector } from "../RangeSelector";

test("RangeSelector should initially return the value", () => {
  const { getByTestId } = render(
    <RangeSelector value={2}>
      {(value) => <p data-testid="value-content">{value}</p>}
    </RangeSelector>
  );

  expect(getByTestId("value-content")).toHaveTextContent("2");
});

test("RangeSelector should re-rerender children when the range value changes", () => {
  const { getByTestId, getByRole, container } = render(
    <RangeSelector value={2}>
      {(value) => <p data-testid="value-content">{value}</p>}
    </RangeSelector>
  );

  expect(getByTestId("value-content")).toHaveTextContent("2");

  fireEvent.change(getByRole("slider"), {
    currentTarget: { value: 3 },
  });

  return wait(
    () => expect(getByTestId("value-content")).toHaveTextContent("3"),
    { container }
  );
});
