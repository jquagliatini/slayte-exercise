import React from "react";

export function RemoteDataProvider({ url, children }) {
  const [remoteData, setRemoteData] = React.useState([]);
  const [errors, setErrors] = React.useState([]);

  React.useEffect(() => {
    async function fetchMissions() {
      try {
        const response = await fetch(new URL(url).href);
        if (!response.ok) {
          setErrors(e =>
            e.concat("There was an error while retrieving the data")
          );
        }

        setRemoteData(await response.json());
      } catch (e) {
        setErrors(e => e.concat(e));
      }
    }

    fetchMissions();
  }, [url]);

  if (remoteData.length === 0 && errors.length === 0) {
    return <div data-testid="loading-placeholder">loading...</div>;
  } else if (errors.length > 0) {
    return (
      <ul
        data-testid="error-content"
        style={{
          listStyle: "none",
          margin: 0,
          padding: "10px",
          color: "#C53030",
          backGroundColor: "#FEB2B2",
          borderRadius: "5px",
          border: "1px solid #C53030",
        }}
      >
        {errors.map((error, index) => (
          <li key={`error_${index}`}>{error.message}</li>
        ))}
      </ul>
    );
  } else {
    return children(remoteData);
  }
}
