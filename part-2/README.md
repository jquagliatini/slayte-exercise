# Part 2

Build a base class that fetches a json resource at a remote location. Extend this base class with two classes, one that renders the response as stringified text with a prop `depth` (`JSON.stringify(object, null, depth`), the other that renders it as a base64 hash.

**Requirements:**

* Ensure to re-fetch if updating the URL in both components; If updating the stringified `depth` parameter, do not refetch but only re-render.
* Render multiple of these components on one page, giving each a different url. Ensure you're actually rendering the correct result per component
* You can choose to use fetch or axios

## Testing

This is supposed to work, but for a reason I can't really find, the waitFor doesn't
word. Anyway, this is how I would implement it.
