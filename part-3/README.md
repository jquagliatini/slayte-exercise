# Part 3

You're adding to a state-less express server. The server instantiates a HANDLER-Class per request and invokes its HANDLE-method, which ensures parameters are in order. The HANDLER has a CONTROLLER-instance, and in the HANDLER’s HANDLE-method the CONTROLLER's method `updateRemoteApi` is invoked.
The CONTROLLER component encapsulates communication with a remote API, which is an old API that is flaky and not well written, but RESTful. The CONTROLLER has to send two consecutive requests on that API to update two resources within its `updateRemoteApi`-method.

Requirements:

    Implement the CONTROLLER's method `updateRemoteApi`. It should retry each communication with the remote API 3 times. The method should send a PUT request with `new1` to `/api/resource1` and afterwards a PUT request with `new2` to `/api/resource2`
    If the request to Resource 2 is considered FAILED after 3 retries, the change/update to Resource 1 must be undone with an appropriate request
    Provide appropriate tests for your solution’s `updateRemoteApi`-method
