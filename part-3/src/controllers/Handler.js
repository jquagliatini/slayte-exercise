const fetch = require("node-fetch");
const fetchRetry = require("fetch-retry")(fetch);

class Handler {
  constructor(baseURL, { delay = 1000 } = {}) {
    this.baseURL = baseURL;
    this.fetchDelay = delay;
  }

  /**
   * The express handler.
   * @param {Express.Request} _
   * @param {Express.Response} res
   * @param {Express.Next} next
   */
  handle(req, res, next) {
    this.updateRemoteApi(req.body)
      .then(() => {
        res.status(204);
      })
      .catch(next);
  }

  async updateRemoteApi({ new1, new2 }) {
    const { new1: new1Was } = await this._getFirstRemote();
    const { new2: new2Was } = await this._getSecondRemote();

    const firstModification = this._updateFirstRemote(new1);
    const secondModification = this._updateSecondRemote(new2);

    await Promise.allSettled([firstModification, secondModification]).then(
      ([first, second]) => {
        if (first.status === "rejected" && second.satus === "rejected") {
          return;
        }

        if (first.status === "rejected") {
          return this._updateSecondRemote(new2Was);
        }

        if (second.status === "rejected") {
          return this._updateFirstRemote(new1Was);
        }
      }
    );
  }

  _buildUrl(url) {
    return this.baseURL ? new URL(url, this.baseURL) : url;
  }

  get retryOn() {
    return function (attempt, error, response) {
      // Attempt starts at 0
      return attempt < 2 && (!!error || response.status > 299);
    };
  }

  /** The interaction layer with the remote service */
  _putRemote(url, body) {
    return fetchRetry(this._buildUrl(url), {
      retryOn: this.retryOn,
      retryDelay: this.fetchDelay,
      method: "PUT",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(body),
    }).then((response) => {
      if (!response.ok) {
        throw new Error();
      }
    });
  }

  _getRemote(url) {
    return fetchRetry(this._buildUrl(url), {
      retryOn: this.retryOn,
      retryDelay: this.fetchDelay,
    }).then((response) => {
      if (!response.ok) {
        throw new Error();
      }

      return response.json();
    });
  }

  /**
   * @param {string} newValue the newValue to push to remote1
   * @return {Promise<void>} resolves if the update is successful,
   * rejects otherwise
   */
  _updateFirstRemote(newValue) {
    return this._putRemote("/api/resource1", { new1: newValue });
  }

  /** @see _updateFirstRemote */
  _updateSecondRemote(newValue) {
    return this._putRemote("/api/resource2", { new2: newValue });
  }

  _getFirstRemote() {
    return this._getRemote("/api/resource1");
  }

  _getSecondRemote() {
    return this._getRemote("/api/resource2");
  }
}

module.exports = { Handler };
