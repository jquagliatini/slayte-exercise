const { rest } = require("msw");
const { setupServer } = require("msw/node");
const { Handler } = require("../../src/controllers/Handler");

let db = {
  new1: "old_new1",
  new2: "old_new2",
};

const putResource = (resource) => (req, res, ctx) => {
  db[resource] = req.body[resource];
  return res(ctx.status(204));
};

const PutResource1Handler = jest.fn(putResource("new1"));
const PutResource2Handler = jest.fn(putResource("new2"));

const server = setupServer(
  rest.get("http://domain.tld/api/resource1", (_, res, ctx) =>
    res(ctx.json({ new1: db.new1 }))
  ),
  rest.put("http://domain.tld/api/resource1", PutResource1Handler),
  rest.get("http://domain.tld/api/resource2", (_, res, ctx) =>
    res(ctx.json({ new2: db.new2 }))
  ),
  rest.put("http://domain.tld/api/resource2", PutResource2Handler)
);

beforeAll(() => server.listen());
afterEach(() => {
  db = {
    new1: "old_new1",
    new2: "old_new2",
  };

  PutResource1Handler.mockClear();

  return server.resetHandlers();
});
afterAll(() => server.close());

test("updateRemoteApi when everything works well", async () => {
  await new Handler("http://domain.tld").updateRemoteApi({
    new1: "new1",
    new2: "new2",
  });

  expect(db.new1).toBe("new1");
  expect(db.new2).toBe("new2");

  expect(PutResource1Handler).toHaveBeenCalledTimes(1);
  expect(PutResource2Handler).toHaveBeenCalledTimes(1);
});

test("updateRemoteApi should reset everything to its original data if there is a failure", async () => {
  const UnavailableHandler = jest.fn((_, res, ctx) =>
    res(
      ctx.status(503),
      ctx.json({
        errors: ["The application is not available"],
        status: 503,
      })
    )
  );

  server.use(rest.put("http://domain.tld/api/resource2", UnavailableHandler));

  await new Handler("http://domain.tld", { delay: 100 }).updateRemoteApi({
    new1: "new1",
    new2: "new2",
  });

  expect(db.new1).toBe("old_new1");
  expect(db.new2).toBe("old_new2");

  // We called resource2 3 times since we make 3 calls
  // in case of failure
  expect(UnavailableHandler).toHaveBeenCalledTimes(3);
  // We do 2 calls, since we need a 2nd call to reset the
  // initial value.
  expect(PutResource1Handler).toHaveBeenCalledTimes(2);
}, 1000);
