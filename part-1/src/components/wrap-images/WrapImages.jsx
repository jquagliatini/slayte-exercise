/** @jsx jsx */
import React from "react";
import PropTypes from "prop-types";
import { css, jsx } from "@emotion/core";

import { Image } from "../image/Image.jsx";

export function WrapImages({ maxImageWidth, imgs }) {
  const wrapper = css({
    display: "flex",
    flexWrap: "wrap",
    listStyle: "none",
    boxSizing: "border-box",
    padding: 0,
    margin: 0,
  });

  const noSpacing = css({
    margin: 0,
    padding: 0
  });

  return (
    <ul css={wrapper}>
      {imgs.map((img) => (
        <li key={img.id} css={noSpacing}>
          <Image src={img.src} width={maxImageWidth} />
        </li>
      ))}
    </ul>
  );
}

WrapImages.propTypes = {
  imgs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      src: PropTypes.string.isRequired,
    })
  ).isRequired,
  maxImageWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};
