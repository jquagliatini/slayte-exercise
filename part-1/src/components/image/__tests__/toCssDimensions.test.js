import { toCssDimension } from "../Image.jsx";

test.each([
  [undefined, undefined],
  [12, "12px"],
  ["12%", "12%"],
  ["12   ", "12px"],
  ["  12%   ", "12%"],
])("toCssDimension(%s) should return '%s' ", (got, expected) => {
  expect(toCssDimension(got)).toBe(expected);
});
