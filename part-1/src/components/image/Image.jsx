/** @jsx jsx */
import React from "react";
import { css, jsx } from "@emotion/core";

/**
 * Converts a unit into a CSS dimension. Basically transforms
 * a number into a 'px' dimension. If the provided dimension
 * already has a unit, then the value is simply returned.
 * @param {number | string | undefined} value the dimension
 * @return {string | undefined}
 */
export function toCssDimension(_value) {
  if (!_value) { return _value; }

  const value = Number(_value) || String(_value).trim();

  // If the provided dimension uses a unit like '%' or 'em'
  // or anything else that is not a digit, then we simply return
  // that dimension. Otherwise, the default dimension uses 'px'.
  return typeof value === "string" && /\D$/.test(value)
    ? value
    : `${value}px`;
}

export function Image({ src, width, height, alt }) {
  const regulatedDimensions = css({
    maxWidth: toCssDimension(width),
    maxHeight: toCssDimension(height),
  });

  return (
    <img
      alt={alt}
      src={src}
      css={regulatedDimensions}
      data-testid="image-container"
    />
  );
}
