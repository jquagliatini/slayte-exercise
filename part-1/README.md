# Part 1

**Requirements:**

* Build an `Image`-component as follows:
    1. If prop `height` is set, the rendered image can max extend to that height. If it is smaller, it should render in its natural size (no zoom)
    2. If prop `width` is set, the same holds for max width.
* Use your `Image`-component to implement an `WrapImages`-component that can render a list of images and wrap them
  1. Allows specifying property `maxImageWidth` which controls the maximum width of each image
  2. If the images in a row exceed the page width, it should break to the next line (and increase the overall component height according to the max height of all the 2nd row images etc.)
  3. Use JS and CSS where appropriate to achieve the desired effects

## Testing

When using JSDOM inside Jest, it's not possible to use `clientHeight` or `clientWidth`
as the component is not really "rendered". According to [this thread][u:thread] on github, and
quite logically, the solution is to use an integration testing solution, like Cypress.

Since we want to test the components in isoltaion, we used [`cypress-react-unit-test`][u:cy-unit].

[u:thread]: https://github.com/testing-library/react-testing-library/issues/353
[u:cy-unit]: https://github.com/bahmutov/cypress-react-unit-test
