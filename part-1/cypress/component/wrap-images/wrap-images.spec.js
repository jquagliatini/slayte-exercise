/* global cy */
import React from "react";
import { mount } from "cypress-react-unit-test";
import { WrapImages } from "../../../src/components/wrap-images/WrapImages.jsx";

import imageContent from "../image/400.png";

describe("WrapImages", () => {
  const listOfImages = Array.from({ length: 3 }).map((_, i) => ({
    id: i,
    src: imageContent,
  }));

  it("should use the `maxImageWidth` prop to control the `maxWidth` of each sub image", () => {
    mount(<WrapImages imgs={listOfImages} maxImageWidth="200" />);

    cy.get("img").should("have.css", "width", "200px");
  });

  it("should wrap to another line if the line exceeds the page width", () => {
    mount(
      <div data-cy="container" style={{ width: "300px" }}>
        <WrapImages imgs={listOfImages} maxImageWidth="125px" />
      </div>
    );

    // We check that the WrapImages did wrap on 2 lines
    // so (125px + 4px (margins)) * 2 = 258px
    cy.get("[data-cy='container']").should("have.css", "height", "258px");
  });
});
