/* global cy */
import React from "react";
import { mount } from "cypress-react-unit-test";
import { Image } from "../../../src/components/image/Image.jsx";

// This image is 400x400 px
import imageSource from "./400.png";

describe("Image", () => {
  it("if prop 'width' is defined the rendered image can max extend to that value", () => {
    mount(<Image width="100" src={imageSource} />);

    cy.get("img").first().should("have.css", "width", "100px");
  });

  it("if prop 'height' is defined the rendered image can max extend to that value", () => {
    mount(<Image height="100" src={imageSource} />);

    cy.get("img").first().should("have.css", "height", "100px");
  });

  it("if prop 'width' is defined to a value > than the image width, then the image takes the image width", () => {
    mount(<Image width="500" src={imageSource} />);

    cy.get("img").first().should("have.css", "width", "400px");
  });

  it("if prop 'height' is defined to a value > than the image height, then the image takes the image height", () => {
    mount(<Image height="500" src={imageSource} />);

    cy.get("img").first().should("have.css", "height", "400px");
  });
});
